%define gitlab_name archive.tar.bz2

Name:		pci
Version:	0.1.0
Release:	1%{?dist}
Summary:	A diagnostic utility for the PCI bus
Source:		https://gitlab.com/ctuffli/%{name}/repository/v%{version}/%{gitlab_name}
License:	BSD
Group:		Utilities/System
BuildRequires:	libpciaccess-devel libxo

%description
pci is a utility for displaying information about PCI devices in the
system as well as reading and writing device registers.

%prep
# Deal with the fact gitlab.com includes the tag hash in the directory name
#   use tar to list the contents of the archive and extract the directory name
%define arch_dir_name %(tar tf %{_sourcedir}/%{gitlab_name} | head -1 | tr -d '/')
%setup -n %{arch_dir_name}

%build
sh autogen.sh
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install

%clean
rm -rf %{buildroot}

%files
%doc README.md INSTALL.md LICENSE
%doc doc/*
%{_bindir}/*
%{_mandir}/*/*
